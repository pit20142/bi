#1 - Apresenta todas as atividades e sua respectivas respostas, realizadas por aluno (passando o aluno como parâmetro)

#Ex.: 

#A resolução das atividades Aluno X são:

#Atividade XXX:  'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua'
#Atividade ZZZ:  'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua'
#Atividade YYY:  'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua'

SELECT Atividade.titulo, Aluno_has_Atividade.resposta FROM Aluno_has_Atividade, Atividade
WHERE Aluno_has_Atividade.Atividade_idAtividade = Atividade.idAtividade 
	AND Aluno_idAluno=1

#2 - Apresenta todos os conteudos lidos por aluno (passando o aluno como parâmetro)

#Ex.: 

#O Aluno X leu os Conteudos:

#'Pentaho X'
#'Testes Y'
#'Infra Z'

SELECT Conteudo.titulo FROM Avaliacao, Conteudo 
WHERE idConteudo = Conteudo_idConteudo 
	AND Aluno_idAluno=1

#3 - Apresenta a quantidade de atividade realizadas por cada aluno de uma determinada turma (passando a turma como parâmetro)

#Ex.: 

#O Aluno X realizou 2 atividades
#O Aluno Y realizou 5 atividades
#O Aluno Z realizou 1 atividades

SELECT Aluno.nome, COUNT(*) FROM Aluno_has_Atividade, Aluno, Turma 
WHERE Aluno.idAluno = Aluno_has_Atividade.Aluno_idAluno
	AND Turma.idTurma=3
	AND Aluno.Turma_idTurma = Turma.idTurma
GROUP BY Aluno_idAluno, Aluno.nome ORDER BY Aluno.nome


#4 - Apresenta a quantidade de conteudos lido por cada aluno

#Ex.: 

#O Aluno X leu 2 links
#O Aluno Y leu 5 links
#O Aluno Z leu 1 links

SELECT Aluno.nome, COUNT(*) FROM Avaliacao, Aluno 
WHERE Aluno.idAluno = Avaliacao.Aluno_idAluno 
GROUP BY Aluno_idAluno, Aluno.nome ORDER BY Aluno.nome

#5 - Apresenta os Conteudos e seus repectivos links ordenados do mais mau avaliado para o melhor avaliado

#Ex.: 

#O Conteudo X 'http://br.lipsum.com/' obteve nota 2
#O Conteudo Y 'http://br.lipsum.com/' obteve nota 5
#O Conteudo Z 'http://br.lipsum.com/' obteve nota 1

SELECT Conteudo.titulo, Conteudo.link, AVG(nota_link) FROM Avaliacao, Conteudo 
WHERE Conteudo_idConteudo = idConteudo 
GROUP BY Conteudo.titulo, Conteudo.link ORDER BY AVG(nota_link) DESC

#6 - O quanto que cada atividade foi realizada

#Ex.: 

#A Atividade X teve 3 envios
#A Atividade Y teve 3 envios
#A Atividade Z teve 3 envios

SELECT Atividade.titulo, COUNT(*) FROM Aluno_has_Atividade, Atividade 
WHERE Atividade.idAtividade = Aluno_has_Atividade.Atividade_idAtividade 
GROUP BY Atividade_idAtividade, Atividade.titulo ORDER BY Atividade.titulo


#7 - Apresenta a quantidade de atividade realizadas por turma (passando a turma como parâmetro)

#Ex.: 

#A Área X teve 2 atividades realizadas
#A Área Y teve 2 atividades realizadas
#A Área Z teve 2 atividades realizadas


SELECT Atividade.titulo, COUNT(*) FROM Aluno_has_Atividade, Atividade, Aluno, Turma 
WHERE Atividade.idAtividade = Aluno_has_Atividade.Atividade_idAtividade
	AND Turma.idTurma=1 
	AND Aluno_has_Atividade.Aluno_idAluno = Aluno.idAluno
	AND Turma.idTurma = Aluno.Turma_idTurma
GROUP BY Atividade_idAtividade, Atividade.titulo ORDER BY Atividade.titulo
