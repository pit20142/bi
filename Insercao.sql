INSERT INTO Orientador(idOrientador, nome) VALUES(1, 'Augusto');
INSERT INTO Orientador(idOrientador, nome) VALUES(2, 'Eduardo');


INSERT INTO Area(idArea, nome) VALUES(1, 'BI');
INSERT INTO Area(idArea, nome) VALUES(2, 'Teste');
INSERT INTO Area(idArea, nome) VALUES(3, 'Desenvolvimento');
INSERT INTO Area(idArea, nome) VALUES(4, 'Requisitos');
INSERT INTO Area(idArea, nome) VALUES(5, 'Infraestrutura');


INSERT INTO Turma(idTurma, nome, ano, semestre, status_2) VALUES(1, 'Turma01', 2010, 2, 1);
INSERT INTO Turma(idTurma, nome, ano, semestre, status_2) VALUES(2, 'Turma02', 2011, 1, 1);
INSERT INTO Turma(idTurma, nome, ano, semestre, status_2) VALUES(3, 'Turma03', 2012, 2, 1);
INSERT INTO Turma(idTurma, nome, ano, semestre, status_2) VALUES(4, 'Turma04', 2013, 1, 1);
INSERT INTO Turma(idTurma, nome, ano, semestre, status_2) VALUES(5, 'Turma05', 2014, 2, 1);


INSERT INTO Orientador_has_Turma(Orientador_idOrientador, Turma_idTurma) VALUES(1,1);
INSERT INTO Orientador_has_Turma(Orientador_idOrientador, Turma_idTurma) VALUES(1,2);
INSERT INTO Orientador_has_Turma(Orientador_idOrientador, Turma_idTurma) VALUES(2,3);
INSERT INTO Orientador_has_Turma(Orientador_idOrientador, Turma_idTurma) VALUES(2,4);
INSERT INTO Orientador_has_Turma(Orientador_idOrientador, Turma_idTurma) VALUES(1,5);



INSERT INTO Aluno(idAluno, Area_idArea, Turma_idTurma, nome, matricula, email, turno, status_2) VALUES(1, 1, 1,'Augusto',20140978, 'exemplo@email.com','manhã', 1);
INSERT INTO Aluno(idAluno, Area_idArea, Turma_idTurma, nome, matricula, email, turno, status_2) VALUES(2, 2, 1,'Renan',20148744, 'exemplo@email.com','tarde', 1);
INSERT INTO Aluno(idAluno, Area_idArea, Turma_idTurma, nome, matricula, email, turno, status_2) VALUES(3, 3, 1,'Thiago',20149876, 'exemplo@email.com','manhã', 1);
INSERT INTO Aluno(idAluno, Area_idArea, Turma_idTurma, nome, matricula, email, turno, status_2) VALUES(4, 4, 1,'Marcos',20140074, 'exemplo@email.com','manhã', 1);
INSERT INTO Aluno(idAluno, Area_idArea, Turma_idTurma, nome, matricula, email, turno, status_2) VALUES(5, 5, 1,'Felipe',2014777, 'exemplo@email.com','tarde', 1);
INSERT INTO Aluno(idAluno, Area_idArea, Turma_idTurma, nome, matricula, email, turno, status_2) VALUES(6, 1, 1,'Marina',20144444, 'exemplo@email.com','manhã', 1);
INSERT INTO Aluno(idAluno, Area_idArea, Turma_idTurma, nome, matricula, email, turno, status_2) VALUES(7, 3, 1,'Juliana',20146655, 'exemplo@email.com','tarde', 1);
INSERT INTO Aluno(idAluno, Area_idArea, Turma_idTurma, nome, matricula, email, turno, status_2) VALUES(8, 3, 1,'Paulo',20140978, 'exemplo@email.com','manhã', 1);
INSERT INTO Aluno(idAluno, Area_idArea, Turma_idTurma, nome, matricula, email, turno, status_2) VALUES(9, 5, 1,'Lucas',20148744, 'exemplo@email.com','tarde', 1);
INSERT INTO Aluno(idAluno, Area_idArea, Turma_idTurma, nome, matricula, email, turno, status_2) VALUES(10, 4, 1,'Pedro',20149876, 'exemplo@email.com','manhã', 1);
INSERT INTO Aluno(idAluno, Area_idArea, Turma_idTurma, nome, matricula, email, turno, status_2) VALUES(11, 2, 1,'Tati',20140074, 'exemplo@email.com','manhã', 1);
INSERT INTO Aluno(idAluno, Area_idArea, Turma_idTurma, nome, matricula, email, turno, status_2) VALUES(12, 1, 1,'Roberta',2014777, 'exemplo@email.com','tarde', 1);
INSERT INTO Aluno(idAluno, Area_idArea, Turma_idTurma, nome, matricula, email, turno, status_2) VALUES(13, 4, 1,'Amanda',20144444, 'exemplo@email.com','manhã', 1);
INSERT INTO Aluno(idAluno, Area_idArea, Turma_idTurma, nome, matricula, email, turno, status_2) VALUES(14, 5, 1,'Renata',20146655, 'exemplo@email.com','tarde', 1);
INSERT INTO Aluno(idAluno, Area_idArea, Turma_idTurma, nome, matricula, email, turno, status_2) VALUES(15, 1, 1,'Celia',20140978, 'exemplo@email.com','manhã', 1);
INSERT INTO Aluno(idAluno, Area_idArea, Turma_idTurma, nome, matricula, email, turno, status_2) VALUES(16, 2, 1,'Eduardo',20148744, 'exemplo@email.com','tarde', 1);
INSERT INTO Aluno(idAluno, Area_idArea, Turma_idTurma, nome, matricula, email, turno, status_2) VALUES(17, 4, 1,'Maria',20149876, 'exemplo@email.com','manhã', 1);
INSERT INTO Aluno(idAluno, Area_idArea, Turma_idTurma, nome, matricula, email, turno, status_2) VALUES(18, 5, 1,'Celina',20140074, 'exemplo@email.com','manhã', 1);
INSERT INTO Aluno(idAluno, Area_idArea, Turma_idTurma, nome, matricula, email, turno, status_2) VALUES(19, 4, 1,'Augustus',2014777, 'exemplo@email.com','tarde', 1);
INSERT INTO Aluno(idAluno, Area_idArea, Turma_idTurma, nome, matricula, email, turno, status_2) VALUES(20, 3, 1,'Antonio',20144444, 'exemplo@email.com','manhã', 1);
INSERT INTO Aluno(idAluno, Area_idArea, Turma_idTurma, nome, matricula, email, turno, status_2) VALUES(21, 2, 1,'Jose',20146655, 'exemplo@email.com','tarde', 1);


INSERT INTO Forum(idForum, titulo, descricao, link) VALUES(1, 'Erro', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ','http://pt.stackoverflow.com/questions/39410/');
INSERT INTO Forum(idForum, titulo, descricao, link) VALUES(2, 'Duvida', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ','http://pt.stackoverflow.com/questions/39419/');
INSERT INTO Forum(idForum, titulo, descricao, link) VALUES(3, 'Erro', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','http://pt.stackoverflow.com/questions/39417/');
INSERT INTO Forum(idForum, titulo, descricao, link) VALUES(4, 'Git', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','http://pt.stackoverflow.com/questions/39416/');
INSERT INTO Forum(idForum, titulo, descricao, link) VALUES(5, 'Wki', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','http://pt.stackoverflow.com/questions/39413/');
INSERT INTO Forum(idForum, titulo, descricao, link) VALUES(6, 'Teste', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','http://pt.stackoverflow.com/questions/39415/');


INSERT INTO Conteudo(idConteudo, Forum_idForum, Area_idArea, link, titulo) VALUES(1, 1, 1,'http://pt.wikipedia.org/wiki/','Pentaho');
INSERT INTO Conteudo(idConteudo, Forum_idForum, Area_idArea, link, titulo) VALUES(2, 2, 2,'http://pt.wikipedia.org/wiki/','TDD');
INSERT INTO Conteudo(idConteudo, Forum_idForum, Area_idArea, link, titulo) VALUES(3, 3, 4,'http://pt.wikipedia.org/wiki/','UML');
INSERT INTO Conteudo(idConteudo, Forum_idForum, Area_idArea, link, titulo) VALUES(4, 4, 5,'http://pt.wikipedia.org/wiki/','Servidor Apache');
INSERT INTO Conteudo(idConteudo, Forum_idForum, Area_idArea, link, titulo) VALUES(5, 5, 3,'http://pt.wikipedia.org/wiki/','Java');
INSERT INTO Conteudo(idConteudo, Forum_idForum, Area_idArea, link, titulo) VALUES(6, 6, 1,'http://pt.wikipedia.org/wiki/','Banco de Dados Estrela');


INSERT INTO Atividade(idAtividade, Conteudo_idConteudo, descricao, titulo) VALUES(1, 1,'Crie um banco de dados relacional','Criação do Banco de Dados');
INSERT INTO Atividade(idAtividade, Conteudo_idConteudo, descricao, titulo) VALUES(2, 1,'Crie um banco de dados Estrela','Criação do Banco de Dados Estrela');
INSERT INTO Atividade(idAtividade, Conteudo_idConteudo, descricao, titulo) VALUES(3, 1,'Crie um banco de dados Dash','PDA');
INSERT INTO Atividade(idAtividade, Conteudo_idConteudo, descricao, titulo) VALUES(4, 2,'Teste o código X','Teste01');
INSERT INTO Atividade(idAtividade, Conteudo_idConteudo, descricao, titulo) VALUES(5, 2,'Teste o código Y','Teste02');
INSERT INTO Atividade(idAtividade, Conteudo_idConteudo, descricao, titulo) VALUES(6, 3,'Crie um diagrama de Classe','Diagrama 01');
INSERT INTO Atividade(idAtividade, Conteudo_idConteudo, descricao, titulo) VALUES(7, 3,'Crie um diagrama de Atividade','Diagrama 02');
INSERT INTO Atividade(idAtividade, Conteudo_idConteudo, descricao, titulo) VALUES(8, 3,'Crie um diagrama de Caso de Uso','Diagrama 03');
INSERT INTO Atividade(idAtividade, Conteudo_idConteudo, descricao, titulo) VALUES(9, 4,'Configure o servidor Apache','Apache');
INSERT INTO Atividade(idAtividade, Conteudo_idConteudo, descricao, titulo) VALUES(10, 4,'Configure o servidor Git','Git');
INSERT INTO Atividade(idAtividade, Conteudo_idConteudo, descricao, titulo) VALUES(11, 4,'Configure o servidor SVN','SVN');
INSERT INTO Atividade(idAtividade, Conteudo_idConteudo, descricao, titulo) VALUES(12, 5,'Crie uma aplicaçao com Hibernate','Hibernate');


INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(1, 1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(1, 2,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(1, 3,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(1, 4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(1, 5,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(1, 6,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(1, 7,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(1, 8,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(1, 9,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(1, 10,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(1, 11,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(1, 12,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(2, 1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(2, 2,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(2, 3,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(2, 4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(2, 5,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(2, 6,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(2, 7,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(2, 8,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(2, 9,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(3, 1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(3, 2,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(3, 3,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(3, 6,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(3, 7,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(3, 8,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(3, 9,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(4, 1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(4, 2,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(4, 3,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(4, 4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(4, 5,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(4, 6,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(4, 7,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(4, 8,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(4, 9,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(4, 10,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(4, 11,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(4, 12,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(5, 1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(5, 2,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(5, 3,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(5, 4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(5, 5,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(5, 11,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(5, 12,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(6, 1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(6, 2,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(6, 3,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(6, 4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(6, 5,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(6, 6,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(6, 7,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(6, 8,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(6, 9,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(6, 10,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(6, 11,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(6, 12,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(7, 1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(7, 2,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(7, 3,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(7, 4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(7, 5,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(7, 7,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(7, 8,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(7, 10,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(7, 12,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(8, 1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(8, 2,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(8, 3,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(8, 4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(8, 5,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(8, 6,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(8, 7,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(8, 8,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(8, 9,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(8, 10,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(8, 11,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(8, 12,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(9, 1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(9, 2,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(9, 3,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(9, 4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(9, 5,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(9, 8,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(9, 9,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(9, 10,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(9, 11,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(9, 12,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(10, 1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(10, 3,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(10, 4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(10, 5,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(10, 6,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(10, 8,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(10, 9,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(10, 10,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(10, 11,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(10, 12,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(11, 1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(11, 2,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(11, 3,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(11, 4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(11, 5,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(11, 6,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(11, 7,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(11, 8,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(11, 9,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(11, 10,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(11, 11,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(11, 12,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(12, 1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(12, 2,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(12, 3,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(12, 4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(12, 5,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(12, 6,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(12, 7,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(12, 8,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(12, 9,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(12, 10,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(12, 11,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(12, 12,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(13, 1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(13, 4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(13, 5,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(13, 6,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(13, 7,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(13, 8,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(13, 9,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(13, 10,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(13, 11,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(13, 12,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(14, 1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(14, 2,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(14, 3,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(14, 4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(14, 5,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(14, 6,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(14, 7,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(14, 8,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(14, 11,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(14, 12,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(15, 1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(15, 3,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(15, 4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(15, 5,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(15, 6,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(15, 7,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(15, 9,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(15, 10,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(15, 12,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(16, 1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(16, 2,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(16, 3,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(16, 4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(16, 5,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(16, 6,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(16, 7,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(16, 8,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(16, 9,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(16, 10,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(16, 11,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(16, 12,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(17, 1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(17, 2,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(17, 3,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(17, 4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(17, 5,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(17, 6,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(17, 7,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(17, 8,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(17, 9,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(17, 10,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(17, 11,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(17, 12,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(18, 1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(18, 2,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(18, 3,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(18, 6,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(18, 7,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(18, 8,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(18, 10,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(18, 11,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(18, 12,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(19, 1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(19, 2,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(19, 3,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(19, 4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(19, 5,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(19, 6,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(19, 7,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(20, 1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(20, 2,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(20, 3,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(20, 4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(20, 6,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(20, 7,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(20, 9,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(20, 10,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(20, 11,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(20, 12,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(21, 1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(21, 2,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(21, 3,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(21, 4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(21, 5,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(21, 6,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(21, 7,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(21, 10,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(21, 11,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO Aluno_has_Atividade(Aluno_idAluno, Atividade_idAtividade, resposta) VALUES(21, 12,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');


INSERT INTO Maquina(idMaquina, so, ram, hd, tela) VALUES(1, 'UBUNTU 14.04','2GB', '1T', '14');
INSERT INTO Maquina(idMaquina, so, ram, hd, tela) VALUES(2, 'UBUNTU 14.04','2GB', '1T', '14');
INSERT INTO Maquina(idMaquina, so, ram, hd, tela) VALUES(3, 'UBUNTU 14.04','2GB', '1T', '14');
INSERT INTO Maquina(idMaquina, so, ram, hd, tela) VALUES(4, 'UBUNTU 14.04','2GB', '1T', '14');
INSERT INTO Maquina(idMaquina, so, ram, hd, tela) VALUES(5, 'UBUNTU 14.04','4GB', '1T', '14');
INSERT INTO Maquina(idMaquina, so, ram, hd, tela) VALUES(6, 'UBUNTU 14.04','2GB', '1T', '14');
INSERT INTO Maquina(idMaquina, so, ram, hd, tela) VALUES(7, 'UBUNTU 14.04','2GB', '1T', '14');
INSERT INTO Maquina(idMaquina, so, ram, hd, tela) VALUES(8, 'UBUNTU 14.04','2GB', '1T', '14');
INSERT INTO Maquina(idMaquina, so, ram, hd, tela) VALUES(9, 'UBUNTU 14.04','2GB', '1T', '14');
INSERT INTO Maquina(idMaquina, so, ram, hd, tela) VALUES(10, 'UBUNTU 14.04','2GB', '1T', '14');
INSERT INTO Maquina(idMaquina, so, ram, hd, tela) VALUES(11, 'UBUNTU 14.04','2GB', '1T', '14');
INSERT INTO Maquina(idMaquina, so, ram, hd, tela) VALUES(12, 'UBUNTU 14.04','2GB', '1T', '14');


INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(1, 1, 1, 10, 0, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(2, 1, 2, 9, 1, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(3, 1, 3, 8, 3, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(4, 1, 4, 7, 4, 30, 5, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(5, 1, 5, 10, 1, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(6, 1, 6, 5, 0, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');

INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(7, 2, 1, 10, 0, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(8, 2, 2, 9, 1, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(9, 2, 3, 8, 3, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(10, 2, 4, 7, 4, 30, 5, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(11, 2, 5, 10, 1, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(12, 2, 6, 5, 0, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');

INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(13, 3, 1, 10, 0, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(14, 3, 2, 9, 1, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(15, 3, 3, 8, 3, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(16, 3, 4, 7, 4, 30, 5, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(17, 3, 5, 10, 1, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(18, 3, 6, 5, 0, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');

INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(19, 4, 1, 10, 0, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(20, 4, 2, 9, 1, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(21, 4, 3, 8, 3, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(22, 4, 4, 7, 4, 30, 5, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(23, 4, 5, 10, 1, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(24, 4, 6, 5, 0, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');

INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(25, 5, 1, 10, 0, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(26, 5, 2, 9, 1, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(27, 5, 3, 8, 3, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(28, 5, 4, 7, 4, 30, 5, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(29, 5, 5, 10, 1, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(30, 5, 6, 5, 0, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');

INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(31, 6, 1, 10, 0, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(32, 6, 2, 9, 1, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(33, 6, 3, 8, 3, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(34, 6, 4, 7, 4, 30, 5, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(35, 6, 5, 10, 1, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(36, 6, 6, 5, 0, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');

INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(37, 7, 1, 10, 0, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(38, 7, 2, 9, 1, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(39, 7, 3, 8, 3, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(40, 7, 4, 7, 4, 30, 5, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(41, 7, 5, 10, 1, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(42, 7, 6, 5, 0, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');


INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(43, 8, 1, 10, 0, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(44, 8, 2, 9, 1, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(45, 8, 3, 8, 3, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(46, 8, 4, 8, 4, 30, 5, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(47, 8, 5, 10, 1, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(48, 8, 6, 5, 0, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');

INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(49, 9, 1, 10, 0, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(50, 9, 2, 9, 1, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(51, 9, 3, 8, 3, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(52, 9, 4, 8, 4, 30, 5, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(53, 9, 5, 10, 1, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(54, 9, 6, 5, 0, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');

INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(55, 10, 1, 10, 0, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(56, 10, 2, 9, 1, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(57, 10, 3, 8, 3, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(58, 10, 4, 8, 4, 30, 5, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(59, 10, 5, 10, 1, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(60, 10, 6, 5, 0, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');

INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(61, 11, 1, 10, 0, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(62, 11, 2, 9, 1, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(63, 11, 3, 8, 3, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(64, 11, 4, 8, 4, 30, 5, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(65, 11, 5, 10, 1, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(66, 11, 6, 5, 0, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');


INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(67, 12, 1, 10, 0, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(68, 12, 2, 9, 1, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(69, 12, 3, 8, 3, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(70, 12, 4, 8, 4, 30, 5, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(71, 12, 5, 10, 1, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(72, 12, 6, 5, 0, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');

INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(73, 13, 1, 10, 0, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(74, 13, 2, 9, 1, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(75, 13, 3, 8, 3, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(76, 13, 4, 8, 4, 30, 5, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(77, 13, 5, 10, 1, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(78, 13, 6, 5, 0, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');

INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(79, 14, 1, 10, 0, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(80, 14, 2, 9, 1, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(81, 14, 3, 8, 3, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(82, 14, 4, 8, 4, 30, 5, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(83, 14, 5, 10, 1, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(84, 14, 6, 5, 0, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');

INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(85, 15, 1, 10, 0, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(86, 15, 2, 9, 1, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(87, 15, 3, 8, 3, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(88, 15, 4, 8, 4, 30, 5, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(89, 15, 5, 10, 1, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(90, 15, 6, 5, 0, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');

INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(91, 16, 1, 10, 0, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(92, 16, 2, 9, 1, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(93, 16, 3, 8, 3, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(94, 16, 4, 8, 4, 30, 5, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(95, 16, 5, 10, 1, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(96, 16, 6, 5, 0, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');

INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(97, 17, 1, 10, 0, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(98, 17, 2, 9, 1, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(99, 17, 3, 8, 3, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(100, 17, 4, 8, 4, 30, 5, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(101, 17, 5, 10, 1, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(102, 17, 6, 5, 0, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');

INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(103, 18, 1, 10, 0, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(104, 18, 2, 9, 1, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(105, 18, 3, 8, 3, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(106, 18, 4, 8, 4, 30, 5, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(107, 18, 5, 10, 1, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(108, 18, 6, 5, 0, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');

INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(109, 19, 1, 10, 0, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(110, 19, 2, 9, 1, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(111, 19, 3, 8, 3, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(112, 19, 4, 8, 4, 30, 5, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(113, 19, 5, 10, 1, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(114, 19, 6, 5, 0, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');

INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(115, 20, 1, 10, 0, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(116, 20, 2, 9, 1, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(117, 20, 3, 8, 3, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(118, 20, 4, 8, 4, 30, 5, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(119, 20, 5, 10, 1, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(120, 20, 6, 5, 0, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');

INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(121, 21, 1, 10, 0, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(122, 21, 2, 9, 1, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(123, 21, 3, 8, 3, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(124, 21, 4, 8, 4, 30, 5, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(125, 21, 5, 10, 1, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');
INSERT INTO Avaliacao(idAvaliacao, Aluno_idAluno, Conteudo_idConteudo, nota_link, 
	nivel_conhecimento, tempo, nivel_conhecimento_posterior, importancia, justificativa, conhecimentos) 
VALUES(126, 21, 6, 5, 0, 30, 3, 5,'Lorem ipsum dolor sit amet.', 'Consectetur adipiscing elit.');