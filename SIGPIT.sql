CREATE TABLE Orientador (
  idOrientador SERIAL,
  nome VARCHAR NULL,
  PRIMARY KEY(idOrientador)
);

CREATE TABLE Area (
  idArea SERIAL,
  nome VARCHAR NULL,
  PRIMARY KEY(idArea)
);

CREATE TABLE Forum (
  idForum SERIAL,
  titulo VARCHAR NULL,
  descricao VARCHAR NULL,
  link VARCHAR NULL,
  PRIMARY KEY(idForum)
);

CREATE TABLE Maquina (
  idMaquina SERIAL,
  so VARCHAR NULL,
  tam VARCHAR NULL,
  hd VARCHAR NULL,
  tela VARCHAR NULL,
  PRIMARY KEY(idMaquina)
);

ALTER TABLE Maquina RENAME COLUMN tam TO ram;


CREATE TABLE Turma (
  idTurma SERIAL,
  nome VARCHAR NULL,
  ano INTEGER  NULL,
  semestre INTEGER  NULL,
  status_2 INTEGER  NULL,
  PRIMARY KEY(idTurma)
);

CREATE TABLE Aluno (
  idAluno SERIAL,
  Area_idArea INTEGER  NOT NULL REFERENCES Area(idArea),
  Turma_idTurma INTEGER  NOT NULL REFERENCES Turma(idTurma),
  nome VARCHAR NULL,
  matricula INTEGER  NULL,
  email VARCHAR NULL,
  turno VARCHAR NULL,
  status_2 INTEGER  NULL,
  PRIMARY KEY(idAluno)
);

CREATE TABLE Conteudo (
  idConteudo SERIAL,
  Forum_idForum INTEGER  NOT NULL REFERENCES Forum(idForum),
  Area_idArea INTEGER  NOT NULL REFERENCES Area(idArea),
  link VARCHAR NULL,
  titulo VARCHAR NULL,
  PRIMARY KEY(idConteudo)
);

CREATE TABLE Atividade (
  idAtividade SERIAL,
  Conteudo_idConteudo INTEGER  NOT NULL  REFERENCES Conteudo(idConteudo),
  descricao VARCHAR NULL,
  titulo VARCHAR NULL,
  PRIMARY KEY(idAtividade)
);

CREATE TABLE Aluno_has_Atividade (
  Aluno_idAluno INTEGER  NOT NULL REFERENCES Aluno(idAluno),
  Atividade_idAtividade INTEGER  NOT NULL REFERENCES Atividade(idAtividade),
  data_2 DATE NULL,
  resposta VARCHAR NULL,
  PRIMARY KEY(Aluno_idAluno, Atividade_idAtividade)
);

CREATE TABLE Aluno_has_Maquina (
  Aluno_idAluno INTEGER  NOT NULL REFERENCES Aluno(idAluno),
  Maquina_idMaquina INTEGER  NOT NULL REFERENCES Maquina(idMaquina),
  PRIMARY KEY(Aluno_idAluno, Maquina_idMaquina)
);

CREATE TABLE Ausencia (
  idAusencia SERIAL,
  Aluno_idAluno INTEGER  NOT NULL REFERENCES Aluno(idAluno),
  data_2 DATE NULL,
  razao VARCHAR NULL,
  PRIMARY KEY(idAusencia)
);

CREATE TABLE Avaliacao (
  idAvaliacao SERIAL,
  Aluno_idAluno INTEGER  NOT NULL REFERENCES Aluno(idAluno),
  Conteudo_idConteudo INTEGER  NOT NULL REFERENCES Conteudo(idConteudo),
  nota_link INTEGER  NULL,
  nivel_conhecimento INTEGER  NULL,
  tempo INTEGER  NULL,
  nivel_conhecimento_posterior INTEGER  NULL,
  importancia INTEGER  NULL,
  justificativa VARCHAR NULL,
  conhecimentos VARCHAR NULL,
  data DATE,
  PRIMARY KEY(idAvaliacao)
);

CREATE TABLE Orientador_has_Turma (
  Orientador_idOrientador INTEGER  NOT NULL REFERENCES Orientador(idOrientador),
  Turma_idTurma INTEGER  NOT NULL REFERENCES Turma(idTurma),
  PRIMARY KEY(Orientador_idOrientador, Turma_idTurma)
);

CREATE TABLE Resposta_Forum(
  idResposta_Forum SERIAL,
  Forum_idForum INTEGER  NOT NULL REFERENCES Forum(idForum),
  resposta VARCHAR NULL,
  PRIMARY KEY (idResposta_Forum)
)